<%--
  Created by IntelliJ IDEA.
  User: john
  Date: 30.07.2015
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
    <title>Registration form</title>
    <script type="text/javascript" src="<c:url value="/res/js/PasswordValidator.js"/>"></script>
</head>
<body>

<h1><spring:message code="client.regFormTitle"/></h1>
<form:form name="myform" method="POST" action="./registration" modelAttribute="registrationModel">
    <table>
        <tr>
            <td><form:label path="address"><spring:message code="client.address"/></form:label></td>
            <td><form:input path="address"/></td>
        </tr>
        <tr>
            <td><form:label path="mail"><spring:message code="client.email"/></form:label></td>
            <td><form:input path="mail"></form:input></td>
        </tr>
        <tr>
            <td><form:label path="phone"><spring:message code="client.phone"/></form:label></td>
            <td><form:input path="phone"/></td>
        </tr>
        <tr>
            <td><form:label path="login"><spring:message code="client.login"/></form:label></td>
            <td><form:input path="login"/></td>
        </tr>
        <tr>
            <div class="fieldWrapper">
                <td><form:label for="pass1" path="password"><spring:message code="client.password"/>
                </form:label></td>
                <td>
                        <form:password path="password" id="pass1" onkeyup="checkPass(); return false;"/>
                    <span id="confirmMessage1" class="confirmMessage"></span>
            </div>
        </tr>
        <tr>
            <div class="fieldWrapper">
                <td><form:label for="pass2" path="password"><spring:message code="client.passwordConfirm"/>
                </form:label></td>
                <td>
                        <form:password path="password" id="pass2" onkeyup="checkPass(); return false;"/>
                    <span id="confirmMessage2" class="confirmMessage"></span>
            </div>
        </tr>
        <tr>
            <td><form:label path="corporation"><spring:message code="client.corporation"/>
                /<spring:message code="client.person"/></form:label></td>
            <td><form:radiobutton path="corporation" value="Corporation"/> <spring:message code="client.corporation"/>
                <form:radiobutton path="corporation" value="Person"/><spring:message code="client.person"/>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="Submit" onclick="return checkPass()"/></td>
        </tr>
    </table>
</form:form>
</body>
</html>
