function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('pass1');
    var pass2 = document.getElementById('pass2');
    //Store the Confimation Message Object ...
    var message1 = document.getElementById('confirmMessage1');
    var message2 = document.getElementById('confirmMessage2');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass1.value.length < 6){
        message1.style.color = badColor;
        message1.innerHTML = "Too Short! 6 characters or more"
        return false;
    }
    else {
        message1.style.color = goodColor;
        message1.innerHTML = "Ok"
    }
    if(pass1.value == pass2.value){
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message2.style.color = goodColor;
        message2.innerHTML = "Ok"
        return true;
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message2.style.color = badColor;
        message2.innerHTML = "Passwords Do Not Match!"
        return false;
    }
}