package com.javastudy.deliveryservice.mvc.controller;

import com.javastudy.deliveryservice.mvc.model.Registration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by john on 21.07.2015.
 */
@Controller
public class RegistrationController {

@RequestMapping(value = "/client/registration", method = RequestMethod.GET)
   // @RequestMapping(method = RequestMethod.GET)
    public ModelAndView showForm() {
        Registration reg1 = new Registration();
        System.out.println(reg1);
        return new ModelAndView("Client/registration", "registrationModel", reg1);

    }


    @RequestMapping(method = RequestMethod.POST)

    public String submit(@ModelAttribute("registrationModel") Registration registration,
                         ModelMap model) {
        System.out.println(registration);
        model.addAttribute("address", registration.getAddress());
        model.addAttribute("mail", registration.getMail());
        model.addAttribute("phone", registration.getPhone());
        model.addAttribute("login", registration.getLogin());
        model.addAttribute("password", registration.getPassword());
        model.addAttribute("corporation", registration.getCorporation());
        return "Client/registrationCompleted";
    }
}
