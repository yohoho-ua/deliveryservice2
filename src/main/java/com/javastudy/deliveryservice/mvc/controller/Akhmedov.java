/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javastudy.deliveryservice.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Администратор
 */
@Controller
public class Akhmedov {
    @RequestMapping(method = RequestMethod.GET, value = "/akhmedov.htm")
    public String getPage() {
        return "akhmedov";
    }
}
